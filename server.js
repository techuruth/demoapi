var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var requestjson = require('request-json');
var cors = require('cors');

var path = require('path');
var bodyParser = require('body-parser');
app.use(cors());
app.use(bodyParser.json());

//Bloque mLab
var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/movimientos?apiKey=2Euek6K4E53ZRnoyseuB6pEpKlc00oL9";
var clienteMlab = requestjson.createClient(urlmovimientosMlab);

//Bloque mongoDB
var mongoClient = require('mongodb').MongoClient;
var urlMongoDB = "mongodb://servermongo:27017/local";

//Bloque PostgreSQL
var pg = require('pg');
var urlUsuarios = "postgres://docker:docker@localhost:5433/bdseguridad";


app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.post('/login', function(req, res) {
  //Crear cliente PostgreSQL
  var postgreClient = new pg.Client(urlUsuarios);
  postgreClient.connect();
  //Asumo que recibo req.body = {login:xxxx,password:yyyy}
  //Hacer consulta
  var query = postgreClient.query('SELECT COUNT(*) FROM usuarios WHERE login=$1 AND password=$2;', [req.body.login, req.body.password], (err,result) => {
    if (err) {
      console.log(err);
      res.send(err);
    } else {
      if (result.rows!=null && result.rows.length>0) {
        console.log(result.rows[0]);
        if (result.rows[0].count>=1) {
          res.send("Login correcto");
        } else {
          res.send("Login incorrecto");
        }
      } else {
        console.log("No se han encontrado resultados.");
        res.send("No se han encontrado resultados.");
      }
    }
  });
  postgreClient.close;
});

app.get('/movimientosMongo', function(req, res){
  mongoClient.connect(urlMongoDB,function(err, db) {
      if (err){
        console.log(err);
      } else {
        console.log("Connected successfully to server to get");
        var col=db.collection('movimientos');
        col.find({}).limit(3).toArray(function(err, docs) {
          res.send(docs);
        });
        db.close;
      }
  });
});

app.post('/movimientosMongo', function(req, res){
  mongoClient.connect(urlMongoDB,function(err, db) {
      if (err){
        console.log(err);
      } else {
        console.log("Connected successfully to server to post");
        var col=db.collection('movimientos');
        col.insertOne({"nombre":"Susanita"},function(err,r) {
          if(err){
            console.log(err);
          }else{
            console.log(r.insertedCount+ ' registros insertados');
          }
        });
        res.send("OK");
        db.close;
      }
  });
});

app.get('/movimientos', function(req, res) {

  //Pongo '' en el get porque uso la urlmovimientosMlab tal cual
  clienteMlab.get('', function(err, resM, body){
    if (err){
      console.log(body);
    } else {
//      console.log(body);
      res.send(body);
    }
  });
});

app.post('/movimientos', function(req, res) {
  var data = {
  idCliente: 0001,
  nombre: "Paco",
  apellido: "Hernandez",
  listado: [
      {
          fecha: "26/11/2017",
          importe: 33.95,
          categoria: "A"
      },
      {
          fecha: "27/11/2017",
          importe: 12.95,
          categoria: "B"
      }
  ]
  };
  var data2 = req.body;
//  console.log(data2);
  clienteMlab.post('',data2, function(err, resM, body){
    if (err){
      console.log(body);
    } else {
      res.send(body);
    }
  });
});

app.post('/', function(req, res) {
  res.send("Hola mundo post");
});

var clientes = [{"name":"Ruth","apellido":"Castrejon","nacionalidad":"ES"},{"name":"JJ","apellido":"Fernandez","nacionalidad":"FR"}];

app.get('/Clientes', function(req, res) {
  res.json(clientes);
});
app.get('/Clientes/:idcliente', function(req, res) {
  res.json(clientes[req.params.idcliente]);
});

app.post('/Clientes/:idcliente', function(req, res) {
  var mensajePost = {"mensaje":"Su cliente "+req.params.idcliente+" ha sido modificado para Docker Cloud"};
  res.json(mensajePost);
});

app.put('/Clientes/:idcliente', function(req, res) {
  var mensajePut = {"mensaje":"Su cliente "+req.params.idcliente+" ha sido creado"};
  res.json(mensajePut);
});

app.delete('/Clientes/:idcliente', function(req, res) {
  var mensajeDelete = {"mensaje":"Su cliente "+req.params.idcliente+" ha sido borrado"};
  res.json(mensajeDelete);
});

app.delete('/Clientes', function(req, res) {
  var mensajeDelete = {"mensaje":"Los clientes han sido borrados"};
  res.json(mensajeDelete);
});
